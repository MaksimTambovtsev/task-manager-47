package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.service.IAuthService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.exception.field.PermissionException;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.exception.system.AccessDeniedException;

import java.util.Arrays;
import java.util.Optional;


public class AuthService implements IAuthService {

    @NotNull
    private IPropertyService propertyService;

    @Nullable
    private String userId;

    @Override
    public void login(@Nullable final String login,
                      @Nullable final String password) {
    }

    @NotNull
    @Override
    public UserDTO getUser() {
        return null;
    }

    @Override
    public @NotNull UserDTO check(@Nullable String login, @Nullable String password) {
        return null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    @Nullable
    public UserDTO registry(@Nullable final String login,
                            @Nullable final String password,
                            @Nullable final String email) {
        return null;
    }

    @NotNull
    @Override
    public String getUserId() {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final UserDTO user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}