package ru.tsc.tambovtsev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskChangeStatusByIdResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIdResponse(@Nullable TaskDTO task) {
        super(task);
    }

}
