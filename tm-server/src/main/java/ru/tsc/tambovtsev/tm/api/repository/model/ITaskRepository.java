package ru.tsc.tambovtsev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    void create(@NotNull Task task);

    void updateById(@NotNull Task task);

    void removeByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    Task findById(@Nullable String userId, @Nullable String id);

    void clear(@Nullable String userId);

    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    List<Task> findAllTask();

    long getSize(@Nullable String userId);

    void removeById(@Nullable String userId, @Nullable String id);

    void clearTask();

}
