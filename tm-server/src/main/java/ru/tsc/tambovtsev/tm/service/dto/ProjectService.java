package ru.tsc.tambovtsev.tm.service.dto;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.dto.IProjectService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;
import ru.tsc.tambovtsev.tm.repository.dto.ProjectRepository;

import javax.persistence.EntityManager;

import java.util.*;

public class ProjectService extends AbstractUserOwnedService<ProjectDTO, IProjectRepository> implements IProjectService {

    public ProjectService(
            @NotNull final IProjectRepository repository,
            @NotNull IConnectionService connection
    ) {
        super(repository, connection);
    }

    @NotNull
    @Override
    public IProjectRepository getRepository(@NotNull EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            final ProjectDTO project = repository.findById(userId, id);
            if (project == null) return null;
            entityManager.getTransaction().begin();
            project.setStatus(status);
            project.setUserId(userId);
            repository.updateById(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    @SneakyThrows
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            @Nullable final ProjectDTO result = repository.findById(userId, id);
            if (result == null) return;
            entityManager.getTransaction().begin();
            repository.removeByIdProject(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(
            @Nullable final String userIdPr,
            @Nullable final Sort sort
    ) {
        Optional.ofNullable(userIdPr).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(sort).orElseThrow(NameEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            return repository.findAll(userIdPr, sort);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            return repository.findAllProject();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final Collection<ProjectDTO> models) {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearProject() {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clearProject();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> models) {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clearProject();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

}
